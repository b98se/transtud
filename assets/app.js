/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//regex validates for either DD/MM/YYYY or DD-MM-YYYY allow dates such as 31/02/4899
var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/](25)\d{2}$/;
var pid13format = /\d{13}/;

function showAlert(typeAlert, txt) {
    
    $('#modalAlert .modal-card-body').html(txt);
    $("#modalAlert").addClass("is-active");
}

function isDate(txtDate)
{
    var currVal = txtDate;
    if(currVal == '')
        return false;

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[3];
    dtDay= dtArray[1];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay> 31)
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
        return false;
    else if (dtYear < 1900)
        return false;
    else if (dtMonth == 2)
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap))
            return false;
    }
    return true;
}

function mysql2thaidate(txtDate) {
    //console.log(txtDate);
    var currVal = txtDate;
    if(currVal == '')
        return '.';

    var rxDatePattern = /^(\d{4})-(\d{1,2})-(\d{1,2})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return '..';

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[2];
    dtDay= dtArray[3];
    dtYear = parseInt(dtArray[1]) + 543;
    //dtYear = parseInt(dtArray[1]);

    return dtDay + '/' + dtMonth + '/' + dtYear;
}