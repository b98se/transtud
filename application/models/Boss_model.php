<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Boss_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_boss($id) {

        $query = $this->db->from('bosss')->where('id', $id)->get();
        return $query->row();
    }

    function fetch_bosss($smis, $active = 'y') {

        $this->db->from('bosss')->where('smis_id', $smis);

        if($active == 'y' || $active == 'n'){
            $this->db->where('active', $active);
        }
        
        $query = $this->db->get();
        return $query->result();
    }

}
