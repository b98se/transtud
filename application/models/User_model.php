<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function chk_username_password($username, $password) {
        $this->db->from('users')
            ->where('username', $username)
            ->where('password', $password)
            ->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $this->User_model->update_lastseen($username);
            return $query->result();
        } else {
            return false;
        }
    }

    public function update_lastseen($username) {

        $data['lastseen'] = date("Ymd-His");
        $this->db->where('username', $username)->update('users', $data);
    }

    function get_user($id) {

        $query = $this->db->from('users')->where('username', $id)->get();
        return $query->row();
    }

    public function fetch_schools() {

        $query = $this->db->from('users')
                ->where('user_type', 's')->get();
        return $query->result();
    }

}
