<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class List_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_list($id) {

        $query = $this->db->from('lists')->where('id', $id)->get();
        return $query->row();
    }

    function fetch_lists($listName) {

        $query = $this->db->from('lists')->where('list_name', $listName)->get();
        return $query->result();
    }

}
