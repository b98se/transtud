<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Move_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_move($id) {

        $query = $this->db->from('moves')->where('id', $id)->get();
        return $query->row();
    }

    function get_move_user_boss($id) {

        $query = $this->db->from('moves')
            ->join('users', 'moves.smis_id=users.username', 'left')
            ->join('bosss', 'moves.sign_boss_id=bosss.id', 'left')
            ->where('moves.id', $id)->get();

        return $query->row();
    }

    function fetch_moves($smis_id) {

        $query = $this->db->from('moves')->where('smis_id', $smis_id)->get();
        return $query->result();
    }
    
    function fetch_move_students($move_id) {

        $query = $this->db->from('move_students')->where('move_id', $move_id)->get();
        return $query->result();
    }
    function get_move_student($id) {

        $query = $this->db->from('move_students')->where('id', $id)->get();
        return $query->row();
    }

}
