<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Address_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_province($id) {

        $query = $this->db->from('provinces')->where('id', $id)->get();
        return $query->row();
    }

    function fetch_provinces() {

        $query = $this->db->from('provinces')->order_by("prov_t", "asc")->get();
        return $query->result();
    }

    function get_amphoe($id) {

        $query = $this->db->from('amphoes')->where('amp_code', $id)->get();
        return $query->row();
    }
    function get_tambon($id) {

        $query = $this->db->from('tambons')->where('tam_code', $id)->get();
        return $query->row();
    }
}
