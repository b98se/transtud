<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in')){
			$d['logged_in'] = true;
		}else{
			$d['logged_in'] = false;
		}
		$this->load->template('login_form_view', $d);

	}

  function login() {
  	
  		$this->load->model('User_model', '', TRUE);
      //Field validation succeeded.  Validate against database
      $username = $this->input->post('usr');
      $password = $this->input->post('pwd');

      //query the database
      $result = $this->User_model->chk_username_password($username, $password);

      if ($result) {
          foreach ($result as $row) {
              $sess_array = array(
                  'id' => $row->id,
                  'username' => $row->username,
                  'smis_id' => $row->username,
                  'name' => $row->name,
                  'user_type' => $row->user_type,
                  'area_id' => $row->area_id,
                  'doc_prefix_no' => $row->doc_prefix_no

              );
              $this->session->set_userdata('logged_in', $sess_array);
          }
          echo "yes";
      } else {
          echo "no";
      }
  }

  function logout() {
      $this->session->unset_userdata('logged_in');
      session_destroy();
      redirect(site_url('/'), 'refresh');
  }

}
