<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('User_model', '', TRUE);

        if($this->session->userdata('logged_in')){
            $this->session_data = $this->session->userdata('logged_in');
        }else{
            $this->session->set_flashdata('alert','danger | กรุณา Login เข้าสู่ระบบ !!! ');
            redirect(site_url('/'));
        }
    }

    public function index() {

        $d['schools'] = $this->User_model->fetch_schools();
        $this->load->template('user_index_view', $d);
    }

    public function show($username) {

        $d['school'] = $this->User_model->get_user($username);
        $this->load->template('user_show_view', $d);
    }

}
