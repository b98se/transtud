<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Boss extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('List_model', '', TRUE);
        $this->load->model('Boss_model', '', TRUE);

        if ($this->session->userdata('logged_in')) {
            $this->session_data = $this->session->userdata('logged_in');
            
        } else {
            $this->session->set_flashdata('alert', 'danger | กรุณา Login เข้าสู่ระบบ !!! ');
            redirect(site_url('/'));
        }
    }

    function index()
    {
        $d['bosss'] = $this->Boss_model->fetch_bosss($this->session_data['smis_id'], 'all');
        $this->load->template('boss_index_view', $d);
    }

    function add(){

        $d['schTypes'] = $this->List_model->fetch_lists('sch_type');
        $d['bosss'] = $this->Boss_model->fetch_bosss($this->session_data['smis_id']);
        $this->load->template('move_add_view', $d);
    }

}
