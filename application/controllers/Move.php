<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Move extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Move_model', '', TRUE);
        $this->load->model('List_model', '', TRUE);
        $this->load->model('Address_model', '', TRUE);
        $this->load->model('Boss_model', '', TRUE);

        if ($this->session->userdata('logged_in')) {
            $this->session_data = $this->session->userdata('logged_in');
            
        } else {
            $this->session->set_flashdata('alert', 'danger | กรุณา Login เข้าสู่ระบบ !!! ');
            redirect(site_url('/'));
        }

        //$this->output->enable_profiler(TRUE);
    }

    function index()
    {
        $d['moves'] = $this->Move_model->fetch_moves($this->session_data['smis_id']);

        $this->load->template('move_index_view', $d);
    }

    function add(){

        $d['smis_id'] = $this->session_data['smis_id'];
        $d['doc_prefix_no'] = $this->session_data['doc_prefix_no'];
        $d['bosss'] = $this->Boss_model->fetch_bosss($this->session_data['smis_id']);
        $this->load->template('move_add_view', $d);
    }

    function insert(){

        $data = $this->input->post();

        $data["insert_at"] = date("Y-m-d H:i:s");
        $data["date_form"] = thaidate2mysql($data["date_form"]);
        //print_r($data); exit;

        $this->db->insert('moves', $data);
    
        if ($this->db->affected_rows() == 0) {
            $this->session->set_flashdata('alert','danger | เกิดปัญหาบางประการ !!! \n' . $this->db->_error_message());
        }
        redirect('/move/', 'refresh');
    }

    function edit($id){
        $d['id'] = $id;
        $d['move'] = $this->Move_model->get_move($id);

        $d['doc_prefix_no'] = $this->session_data['doc_prefix_no'];
        $d['bosss'] = $this->Boss_model->fetch_bosss($this->session_data['smis_id']);
        $this->load->template('move_edit_view', $d);
    }

    function update(){
        $d = $this->input->post();

        $d["date_form"] = thaidate2mysql($d["date_form"]);

        $this->db->where('id', $d['id']);
        $this->db->update('moves',$d); 

        if ($this->db->affected_rows() == 0) {
            $this->session->set_flashdata('alert','danger | เกิดปัญหาบางประการ !!! \n' . $this->db->_error_message());
        }
        redirect('/move/', 'refresh');
    }

    function delete($id){
        $this->db->delete('moves', array('id' => $id));
        if($this->db->affected_rows() == 0){
            $this->session->set_flashdata('alert','danger | เกิดข้อผิดพลาดในการลบข้อมูล !!! ');
        }

        redirect(site_url('/move/'), 'refresh');
    }

    function print1($id){

        $d['move'] = $this->Move_model->get_move_user_boss($id);
        $this->load->view('move_print_view', $d);
    }

    function list_student($id){
       
        $d['move'] = $this->Move_model->get_move($id);
        $d['move_students'] = $this->Move_model->fetch_move_students($id);

        $stud_levels = $this->List_model->fetch_lists('stud_level');
        $d['stud_levels'] = array();
        foreach ($stud_levels as $sl) {
            $d['stud_levels'][$sl->list_key] = $sl->list_value;
        }

        $this->load->template('move_student_list_view', $d);
    }


    function add_student($id){

        $d['move_id'] = $id;
        $d['schTypes'] = $this->List_model->fetch_lists('sch_type');
        $d['provinces'] = $this->Address_model->fetch_provinces();
        $d['stud_levels'] = $this->List_model->fetch_lists('stud_level');
        
        $this->load->template('move_student_add_view', $d);
    }

    function insert_student(){

        //print_r($this->input->post());
        $data = $this->input->post();

        $data["bdate"] = thaidate2mysql($data["bdate"]);
        $data["receive"] = thaidate2mysql($data["receive"]);

        $this->db->insert('move_students', $data);
    
        if ($this->db->affected_rows() == 0) {
            $this->session->set_flashdata('alert','danger | เกิดปัญหาบางประการ !!! \n' . $this->db->_error_message());
        }
        redirect(site_url('/move/list_student/'.$data["move_id"]), 'refresh');
    }

    function edit_student($id){

        $d['id'] = $id;
        $d['move_student'] = $this->Move_model->get_move_student($id);

        $d['sch_amp_name'] = $this->Address_model->get_amphoe($d['move_student']->sch_amp_code);
        $d['sch_tam_name'] = $this->Address_model->get_tambon($d['move_student']->sch_tam_code);

        $d['schTypes'] = $this->List_model->fetch_lists('sch_type');
        $d['provinces'] = $this->Address_model->fetch_provinces();
        $d['stud_levels'] = $this->List_model->fetch_lists('stud_level');
        
        $this->load->template('move_student_edit_view', $d);
    }

    function update_student(){
        print_r($this->input->post());
        $d = $this->input->post();

        $this->db->where('id', $d['id']);
        
        unset($d['id']);
        $this->db->update('move_students', $d);

        redirect(site_url('/move/list_student/'.$d["move_id"]), 'refresh');
    }

    function del_student($move_id, $id){

        $this->db->delete('move_students', array('id' => $id));
        if($this->db->affected_rows() == 0){
            $this->session->set_flashdata('alert','danger | เกิดข้อผิดพลาดในการลบข้อมูล !!! ');
        }

        redirect(site_url('/move/list_student/'.$move_id), 'refresh');
    }
}

