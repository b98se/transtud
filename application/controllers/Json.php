<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Json extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //$this->load->model('Move_model', '', TRUE);
        //$this->load->model('List_model', '', TRUE);
        $this->load->model('Address_model', '', TRUE);
        //$this->load->model('Boss_model', '', TRUE);

    }

    function index()
    {
        
    }

    function amphoes($prov_code){

        $query = $this->db->from('amphoes')
                        ->where('prov_code', $prov_code)
                        ->order_by('amp_t')->get();
        $amphoes = $query->result();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($amphoes));
    }

    function tambons($amp_code){

        $query = $this->db->from('tambons')
                        ->where('amp_code', $amp_code)
                        ->order_by('tam_t')->get();
        $tambons = $query->result();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($tambons));
    }

    function schoolsbyzip($zip_code){

        $query = $this->db->from('schools')
                        ->where('zipcode', $zip_code)
                        ->order_by('name')->get();
        $schools = $query->result();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($schools));
    }
}
