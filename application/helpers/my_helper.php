<?php
function mysql2thaidate($date, $format = 'short', $isBuddhistEra = true, $num = false){
    $date = new DateTime($date);
    //$result = $date->format('Y-m-d H:i:s');
    $year = $date->format('Y');
    if($isBuddhistEra){
        $year += 543;
    }
    $Y['long'] = $year;
    $Y['short'] = substr($year, -2);
    $m = $date->format('m');
    $d = $date->format('d');

    $month = array(
        'long' => array('01'=>'มกราคม','02'=>'กุมภาพันธ์','03'=>'มีนาคม','04'=>'เมษายน','05'=>'พฤษภาคม','06'=>'มิถุนายน','07'=>'กรกฏาคม','08'=>'สิงหาคม','09'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤษจิกายน','12'=>'ธันวาคม'),
        'short' => array('01'=>'ม.ค.','02'=>'ก.พ.','03'=>'มี.ค.','04'=>'เม.ย.','05'=>'พ.ค.','06'=>'มิ.ย.','07'=>'ก.ค.','08'=>'ส.ค.','09'=>'ก.ย.','10'=>'ต.ค.','11'=>'พ.ย.','12'=>'ธ.ค.')
    );

    if ($num == true){
        return $d.'/'.$m.'/'.$Y[$format];
    }else{
        return $d.' '.$month[$format][$m].' '.$Y[$format];
    }
}
function thaidate2mysql($date){

    list($d,$m,$Y) = explode('/',$date);
	$d = str_pad($d,2,"0",STR_PAD_LEFT);
	$m = str_pad($m,2,"0",STR_PAD_LEFT);
    $Y = $Y-543;
    //return $Y.'-'.$m.'-'.$d;
    return $Y.$m.$d;
}

function get_percentage($total, $number)
{
    if ( $total > 0 ) {
        return round($number / ($total / 100),0);
    } else {
        return 0;
    }
}
function set_grade($score){

    if( $score >= 80 ){
        return "success";
    }else if( $score >= 60 && $score < 80 ){
        return "info";
    }else if( $score >= 50 && $score < 60 ){
        return "primary";
    }else if( $score >= 10 && $score < 50 ){
        return "warning";
    }else{
        return "danger";
    }
}