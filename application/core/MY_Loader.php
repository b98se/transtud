<?php

/**
 * /application/core/MY_Loader.php
 *
 */
class MY_Loader extends CI_Loader {

    public function template($template_name, $vars = array(), $return = FALSE) {
        $CI = & get_instance();
        /**/
        if ($CI->session->userdata('logged_in')) {
            $session_data = $CI->session->userdata('logged_in');
            $vars['username'] = $session_data['username'];
            $vars['name'] = $session_data['name'];
        } else {
            $vars['username'] = $vars['name'] = '';
        }

        if ($return):
            $content = $this->view('template/head', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('template/foot', $vars, $return);

            return $content;
        else:
            $this->view('template/head', $vars);
            $this->view($template_name, $vars);
            $this->view('template/foot', $vars);
        endif;
    }

}
