
<div class="field">
  <label class="label">เลขที่หนังสือส่ง </label>
      <div class="control">
        <input name="doc_no" id="doc_no" class="input" type="text" required="" value="<?php echo $doc_prefix_no ?>/">
      </div>
</div>

<div class="field">
  <label class="label">วันที่หนังสือส่ง</label>
  <div class="control">
    <input name="date_form" id="date_form" class="input" type="text" required="">
  </div>
  <p class="help is-success">ตัวอย่าง เช่น 25/02/2562</p>
</div>

<div class="field">
  <label class="label">ผู้ลงนามหนังสือส่ง</label>
  <div class="control">
    <div class="select" >
      <select name="sign_boss_id">        
        <?php 
        foreach ($bosss as $b) {
          echo sprintf("<option value='%s' >%s (%s)</option>", $b->id, $b->b_name, $b->b_position);
        }
        ?>
      </select>
    </div>
  </div>
</div>


<script type="text/javascript">

$("#form_add_move").submit(function(){
    
    //console.log("dateformat test " + dateformat.test($("#bdate").val()));

    if(!dateformat.test($("#date_form").val())){
        showAlert('danger', 'วันเดือนปี รูปแบบไม่ถูกต้อง');
        return false;
    }

});

</script>