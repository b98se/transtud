<section class="section">
	<div class="container">
    <h1 class="title">แบบ พฐ.19/2 (1)</h1>
    <h2 class="subtitle">
      แบบรายงานผลการรับนักเรียนกรณีมีนักเรียนย้ายไปเข้าเรียน</h2> 

<form method="post" action="<?php echo site_url('move/update') ?>" id="form_add_move" name="form_add_move">

<div class="columns">
<div class="column is-half is-offset-one-quarter">

<?php $this->load->view('move_form_view');?>

  <div class="control">
    <input type="hidden" id="id" name="id" value="<?php echo $id ?>">
    <button class="button is-primary">บันทึก</button>
    <a class="button is-danger" href="<?php echo site_url('move/delete/').$id ?>" onclick="return confirm(' คุณต้องการลบข้อมูลนี้ ใช่หรือไม่ ? ');">ลบ</a>
  </div>


</div> <!-- class="column is-half is-offset-one-quarter" -->
</div> <!-- class="columns" -->

</form>
</div>
</section>

<script type="text/javascript">
  
$(function() {

var fieldexclude = [ ];

var js_data = '<?php echo json_encode($move); ?>';
var js_obj_data = JSON.parse(js_data );

console.log(js_data);
/**/
$.each( js_obj_data, function( key, value ) {

  if(key == 'date_form'){
      value = mysql2thaidate(value);
  }
  if(fieldexclude.indexOf(key) == '-1'){
    $("#" + key).val(value);
  }
});

});
</script>