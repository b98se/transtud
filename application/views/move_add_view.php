<section class="section">
	<div class="container">
    <h1 class="title">แบบ พฐ.19/2 (1)</h1>
    <h2 class="subtitle">
      แบบรายงานผลการรับนักเรียนกรณีมีนักเรียนย้ายไปเข้าเรียน</h2> 

<form method="post" action="<?php echo site_url('move/insert') ?>" id="form_add_move" name="form_add_move">

<div class="columns">
<div class="column is-half is-offset-one-quarter">

<?php $this->load->view('move_form_view');?>

  <div class="control">
    <input type="hidden" id="smis_id" name="smis_id" value="<?php echo $smis_id ?>">
  <button class="button is-primary">บันทึก</button>
  </div>


</div> <!-- class="column is-half is-offset-one-quarter" -->
</div> <!-- class="columns" -->

</form>
</div>
</section>