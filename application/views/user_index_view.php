
<section class="section">
  <div class="container">
    <h1 class="title">รายการขอย้ายเข้า</h1>
    <h2 class="subtitle">A simple container to divide your page into </h2> 

    <link rel="stylesheet" type="text/css" href='<?php echo base_url("assets"); ?>/datatables.min.css' >
    <script type="text/javascript" src='<?php echo base_url("assets"); ?>/datatables.min.js'></script>

    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>ที่</th>
                <th>รหัส smis</th>
                <th>ชื่อโรงเรียน</th>
                <th>officer_name</th>
                <th>officer_telno</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php 

            foreach ($schools as $i => $sch) {

                $tr = "<tr>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                </tr>";

                $showLink = "<a class='button is-success is-small' href='" .site_url('user/show/') . $sch->username. "'>ดู</a>";

                echo sprintf($tr, ($i+1), $sch->username, $sch->name, $sch->officer_name, $sch->officer_telno, $showLink);
            }
            ?>
        </tbody>
    </table>

  </div>

</section>

<script type="text/javascript">
  $(document).ready(function() {
      $('#example').DataTable();
  } );

</script>
