<section class="section">
  <div class="container">
    <h1 class="title">รายการ พฐ.19/2 (1)</h1>
    <h2 class="subtitle">A simple container to divide your page into </h2> 

    <link rel="stylesheet" type="text/css" href='<?php echo base_url("assets"); ?>/datatables.min.css' >
    <script type="text/javascript" src='<?php echo base_url("assets"); ?>/datatables.min.js'></script>


    <article class="message is-info">
      <div class="message-header">
        <p>รายละเอียดหนังสือนำส่ง พฐ.19/2 (1)</p>
      </div>
      <div class="message-body">
        <ul>
            <li>ที่ <?php echo $move->doc_no ?></li>
            <li>วันที่ <?php echo mysql2thaidate($move->date_form) ?></li>
        </ul>
      </div>
    </article>

    <a href="<?php echo site_url("move/add_student/").$move->id ?>" class="button is-primary"  id="showModal">เพิ่มข้อมูลนักเรียน</a>

    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>เลข ปชช.</th>
                <th>ชื่อ-สกุล</th>
                <th>วันเกิด</th>
                <th>ระดับชั้น</th>
                <th>ชื่อผู้ปกครอง</th>
                <th>วันที่รับเข้า</th>
                <th>จากโรงเรียน</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php 
/*
  `person_id` varchar(13) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `bdate` date DEFAULT NULL,
  `study_level` varchar(2) DEFAULT NULL,
  `parent_name` varchar(255) DEFAULT NULL,
  `receive` date DEFAULT NULL,
  `sch_moecode` varchar(20) DEFAULT NULL,
  `sch_name` varchar(255) DEFAULT NULL,
  `sch_moo` varchar(255) DEFAULT NULL,
  `sch_tam_code` int(11) DEFAULT NULL,
  `sch_amp_code` int(11) DEFAULT NULL,
  `sch_prov_code` int(11) DEFAULT NULL,
  `sch_type` varchar(255) DEFAULT NULL,
*/  
            $html ="<tr>
                <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td>  <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> 
            </tr> \n";

            foreach ($move_students as $i => $m) {
                //$m->date_form = 888;
                $m->bdate = mysql2thaidate($m->bdate);
                $m->receive = mysql2thaidate($m->receive);

                $link_html = "<a href='%s' >แก้ไข</a> <a href='%s' onclick=\"return confirm(' คุณต้องการลบข้อมูลนี้ ใช่หรือไม่ ? ');\">ลบ</a> ";
                $link = sprintf($link_html, site_url('/move/edit_student/'.$m->id), site_url('/move/del_student/'.$move->id.'/'.$m->id));

                echo sprintf($html, ($i+1), $m->person_id, $m->name, $m->bdate, $stud_levels[$m->study_level], $m->parent_name, $m->receive, $m->sch_name, $link );
            }

             ?>
            
        </tbody>
    </table>

  </div>

</section>



<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
  } );

</script>
