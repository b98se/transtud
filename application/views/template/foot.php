<footer class="footer">
  <div class="content has-text-centered">
    <p>
      <strong>Bulma</strong> by <a href="https://jgthms.com">Jeremy Thomas</a>. The source code is licensed
      <a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
      is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY NC SA 4.0</a>.
    </p>
  </div>
</footer>

<script>
$(function () {
  <?php
  if($this->session->flashdata('alert')){
      $d = explode('|', $this->session->flashdata('alert'));
      echo "showAlert('".$d[0]."', ' ".$d[1]." ');";
  }
  ?>

  $("#modalAlertClose").click(function(){
    $("#modalAlert").removeClass("is-active");
  });
});
</script>

</body>
</html>