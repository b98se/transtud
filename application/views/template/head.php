﻿<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> <?php echo SYS_NAME ?> : <?php echo SYS_OFFICE_NAME_SHORT ?> </title>

    <link rel="stylesheet" href='<?php echo base_url("assets"); ?>/bulma.min.css' >
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
    <link rel="stylesheet" href='<?php echo base_url("assets"); ?>/app.css' >

    <script src='<?php echo base_url("assets"); ?>/jquery-3.3.1.min.js' ></script>
    <script src='<?php echo base_url("assets"); ?>/app.js' ></script>



  </head>
  <body>

  <section class="hero is-primary is-bold is-small">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        <?php echo SYS_NAME ?>
      </h1>
      <h2 class="subtitle">
        <?php echo SYS_OFFICE_NAME_SHORT ?> 
      </h2>
    </div>
  </div>
  </section>

<?php 
if(!empty($name)){
  $this->load->view('template/nav'); 
}
?>

<div class="modal" id="modalAlert">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">แจ้งเตือน</p>
    </header>
    <section class="modal-card-body">
      <!-- Content ... -->
    </section>
    <footer class="modal-card-foot">
      <button class="button is-danger" id="modalAlertClose">ปิด</button>
    </footer>
  </div>
</div>
