<nav class="navbar is-dark" role="navigation" aria-label="main navigation ">
<div class="container">

    <div class="navbar-brand">
    <!-- navbar items, navbar burger... -->
      <a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false"> 
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>  
  </div>

  <div class="navbar-menu" id="navMenu">
    <div class="navbar-start">
      <a class="navbar-item" href="<?php echo site_url('/') ?>">
        หน้าแรก
      </a>

      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          พฐ.19/2 (1)
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item" href="<?php echo site_url('move/') ?>">
            รายการ พฐ.19/2 (1)
          </a>
          <a class="navbar-item" href="<?php echo site_url('move/add') ?>">
            บันทึก พฐ.19/2 (1)
          </a>
        </div>
      </div>

      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          ตั้งค่า
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item" href="<?php echo site_url('move/') ?>">
            ผู้ใช้
          </a>
          <a class="navbar-item" href="<?php echo site_url('boss/') ?>">
            ผู้ลงนาม
          </a>
        </div>
      </div>
    </div>

    <div class="navbar-end">
    <?php if(!empty($name)): ?>
      <div class="navbar-item">
        <div class="buttons has-addons">
          <a class="button is-light" href="<?php echo site_url('user/show/').$username ?>"><?php echo $name; ?></a>
          <a class="button is-danger" href="<?php echo site_url('welcome/logout') ?>" >ออก</a>
        </div>
      </div>

    <?php endif; ?>
    </div>


  </div>
</div>  
</nav>

<script type="text/javascript">
  
  $(document).ready(function() {

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      $(".navbar-burger").toggleClass("is-active");
      $(".navbar-menu").toggleClass("is-active");

  });
});
</script>