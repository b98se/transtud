<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> <?php echo SYS_NAME ?> : <?php echo SYS_OFFICE_NAME_SHORT ?> </title>

    <link rel="stylesheet" href='<?php echo base_url("assets"); ?>/bulma.min.css' >
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" 
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
    <link rel="stylesheet" href='<?php echo base_url("assets"); ?>/app.css' >

    <script src='<?php echo base_url("assets"); ?>/jquery-3.3.1.min.js' ></script>
    <script src='<?php echo base_url("assets"); ?>/app.js' ></script>

  </head>
  <body>