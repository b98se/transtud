<section class="section">
  <div class="container">
    <h1 class="title">เพิ่มข้อมูลนักเรียน</h1>
    <h2 class="subtitle">A simple container to divide your page into </h2> 


<form method="post" action="<?php echo site_url('move/insert_student') ?>" name="move_student" id="move_student">

  <?php $this->load->view('move_student_form_view');?>
         
  <input type="hidden" id="move_id" name="move_id" value="<?php echo $move_id ?>">
  <button class="button is-primary">บันทึก</button>
</form>

</div>
</section>

<script type="text/javascript">

</script>