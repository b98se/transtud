
<section class="section">
  <div class="container">
    <h1 class="title">รายการ พฐ.19/2 (1)</h1>
    <h2 class="subtitle">A simple container to divide your page into </h2> 

    <link rel="stylesheet" type="text/css" href='<?php echo base_url("assets"); ?>/datatables.min.css' >
    <script type="text/javascript" src='<?php echo base_url("assets"); ?>/datatables.min.js'></script>

    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>id</th>
                <th>เลขที่หนังสือนำส่ง</th>
                <th>ลงวันที่</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php 

            $link = "<div class='dropdown is-hoverable'>
                              <div class='dropdown-trigger'>
                                <button class='button is-small' aria-haspopup='true' aria-controls='dropdown-menu'>
                                  <span>เมนู</span>
                                </button>
                              </div>
                              
                              <div class='dropdown-menu' id='dropdown-menu' role='menu'>
                                <div class='dropdown-content'>
                                  <a href='" .site_url("move/list_student/"). "%s' class='dropdown-item'> รายชื่อ นร. </a>
                                  <a href='" .site_url("move/edit/"). "%s' class='dropdown-item'> แก้ไข / ลบ </a>
                                  <hr>
                                  <a href='" .site_url("move/print/"). "%s' class='dropdown-item'> Print หนังสือนำส่ง </a>
                                  <a href='" .site_url("move/print_student/"). "%s' class='dropdown-item'> Print รายชื่อนักเรียน </a>

                                </div>
                              </div>
                            </div>";

            $html ="<tr>
                <td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> 
            </tr>";

            foreach ($moves as $m) {
                
                $m->date_form = mysql2thaidate($m->date_form);
                $link = sprintf($link, $m->id, $m->id, $m->id, $m->id);

                echo sprintf($html, $m->id, $m->doc_no, $m->date_form, $link);
            }

             ?>
            
        </tbody>
    </table>

  </div>

</section>

<script type="text/javascript">
  $(document).ready(function() {
      $('#example').DataTable();
  } );

</script>