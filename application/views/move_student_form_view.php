
<div class="card">
<div class="card-header">
  <p class="card-header-title">ข้อมูลนักเรียน </p>
</div>

<div class="card-content">

<div class="columns">
<div class="column ">
        <div class="field">
            <label class="label">เลข ปชช.</label>
            <div class="control">
                <input class="input" type="text" name="person_id" id="person_id" required="" maxlength="13">
            </div>
            <p class="help is-success">กรอกเฉพาะตัวเลข โดยไม่ต้องใส่ - </p>
        </div>
</div> <!-- column -->        
<div class="column ">          
        <div class="field">
            <label class="label">ชื่อ-สกุล นักเรียน</label>
            <div class="control">
                <input class="input" type="text" name="name" id="name" required="">
            </div>
        </div>
</div> <!-- column -->

</div> <!-- columns -->

<div class="columns">
  <div class="column ">          
          <div class="field">
              <label class="label">วันเดือนปีเกิด</label>
              <div class="control">
                  <input name="bdate" id="bdate" class="input" type="text" required="">
              </div>
              <p class="help is-success">ตัวอย่าง เช่น 25/02/2552</p>
          </div>
  </div> <!-- column -->
  <div class="column">
        <div class="field">
            <label class="label">ชื่อ-สกุล ผู้ปกครอง</label>
            <div class="control">
                <input class="input" type="text" name="parent_name" id="parent_name" required="">
            </div>
        </div>
    </div> <!-- column -->   

</div> <!-- columns -->

</div> <!-- class="card-content" -->
</div> <!-- class="card" -->

<hr>

<div class="card">
<div class="card-header">
  <p class="card-header-title">ข้อมูลโรงเรียนที่ขอย้ายออก </p>
</div>

<div class="card-content">

<div class="columns">
  <div class="column">
    <div class="field">
      <label class="label">จังหวัด</label>
      <div class="control">
        
        <div class="select" >
        <select name="sch_prov_code" id="sch_prov_code">       
           <option>กรุณาเลือก</option>
          <?php 
          foreach ($provinces as $p) {
            echo sprintf("<option value='%s' >%s</option>", $p->prov_code, $p->prov_t);
          }
          ?>
        </select>
      </div>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="field">
      <label class="label">อำเภอ</label>
      <div class="control">
        <div class="select" >
        <select name="sch_amp_code" id="sch_amp_code">    
          <option></option>    
        </select>
      </div>

      </div>
    </div>
  </div>
  <div class="column">
    <div class="field">
      <label class="label">ตำบล</label>
      <div class="control">

          <div class="select" >
          <select name="sch_tam_code" id="sch_tam_code">    
            <option></option>    
          </select>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="columns">
  <div class="column is-two-thirds">
    <label class="label">โรงเรียน</label>
    <div class="field has-addons ">
        
        <p class="control">
            <input type="text" id="sch_moecode" name="sch_moecode" class="input" readonly="" size="10">
        </p> 
        <p class="control is-expanded">
            <input name="sch_name" class="input" type="text" required="" id="sch_name">
        </p> 
        <p class="control">
            <a class="button is-primary" id="showModal">เลือก</a>
        </p> 
        <p class="control">
            <a class="button is-danger" id="clearCodeName">ล้าง</a>
        </p> 
      
    </div>
    <p class="help is-success">หากไม่มีรายชื่อให้เลือก ให้พิมพ์ชื่อ</p>
  </div>
  <div class="column">
    <div class="field">
      <label class="label">หมู่ที่</label>
      <div class="control">
        <input name="sch_moo" id="sch_moo" class="input" type="text" placeholder="" required="">
      </div>
    </div>
  </div>
</div>

<div class="columns">
  <div class="column">

    <div class="field">
        <label class="label">ชั้นเรียน</label>
        <div class="control">
            
            <div class="select" >
                <select name="study_level" id="study_level">
                <option>กรุณาเลือก</option>

                <?php 
                foreach ($stud_levels as $sts) {
                echo sprintf("<option value='%s' data-value='%s'>%s</option>", $sts->list_key, $sts->opt1, $sts->list_value);
                }

                ?>
                </select>
            </div>
        </div>
    </div>
  </div> <!-- column -->

  <div class="column">
  <div class="field">
    <label class="label">สังกัดที่นักเรียนขอย้ายออก </label>
        <div class="control">
          <div class="select" >
            <select name="sch_type" id="sch_type">
              <option>กรุณาเลือกสังกัด</option>
              
              <?php 
              foreach ($schTypes as $sts) {
                echo sprintf("<option value='%s' data-value='%s'>%s</option>", $sts->list_key, $sts->opt1, $sts->list_value);
              }

              ?>
            </select>
          </div>
        </div>
  </div>
  </div>
</div>
</div> <!-- class="card-content" -->
</div> <!-- class="card" -->

<hr>
<div class="field">
  <label class="label">วันเดือนปีรับเข้าเรียน</label>
  <div class="control">
      <input name="receive" id="receive" class="input" type="text" required="">
  </div>
  <p class="help is-success">ตัวอย่าง เช่น 25/02/2552</p>
</div>

<div class="modal" id="modalSchool">
<div class="modal-background"></div>
<div class="modal-card">
  <header class="modal-card-head">
    <p class="modal-card-title">ชื่อโรงเรียน</p>
  </header>
  <section class="modal-card-body">
    <!-- Content ... -->
    กรุณาเลือกจังหวัด / อำเภอ / ตำบล
  </section>
  <footer class="modal-card-foot">
    <button class="button" id="btn-modal-close">ปิด</button>
  </footer>
</div>
</div>

<script type="text/javascript">
 
$(function() { 
    $("#sch_type").change(function(){

        v = $(this).find(':selected').data('value')
        console.log('sch_type=' + v);
    });

    $("#sch_prov_code").change(function(){

        v = $(this).find(':selected').val();
        //console.log('sch_prov_code=' + v);

        $.getJSON( "<?php echo site_url('json/amphoes/') ?>" + v, function( data ) {

          var opt = '<option>กรุณาเลือก</option>';
          $.each( data, function( i, d ) {
            opt += "<option value='" + d.amp_code + "'>" + d.amp_t + "</option>" ;
          });
          //console.log('amphoes option =' + opt);

          $("#sch_amp_code").html(opt);
          $("#modalSchool .modal-card-body").html('กรุณาเลือกจังหวัด / อำเภอ / ตำบล');

        });
    });

    $("#sch_amp_code").change(function(){

        v = $(this).find(':selected').val();
        //console.log('sch_amp_code=' + v);

        $.getJSON( "<?php echo site_url('json/tambons/') ?>" + v, function( data ) {

          var opt = '<option>กรุณาเลือก</option>';
          $.each( data, function( i, d ) {
            opt += "<option value='" + d.tam_code + "' data-zipcode='" + d.zipcode + "'>" + d.tam_t + "</option>" ;
          });
          //console.log('amphoes option =' + opt);

          $("#sch_tam_code").html(opt);

        });
    });

    $("#sch_tam_code").change(function(){

        v = $(this).find(':selected').data('zipcode');
        //console.log('sch_tam_code=' + v);

        $.getJSON( "<?php echo site_url('json/schoolsbyzip/') ?>" + v, function( data ) {

          var opt = '';
          $.each( data, function( i, d ) {
            opt += "<li><a class='select-school' data-moecode='" + d.moecode + "' >" + d.name + "</a></li>" ;
          });
          //console.log('schools =' + opt);

          $("#modalSchool .modal-card-body").html("<ul>" + opt + "</ul>");

        });
    });

    //$( ".select-school" ).delegate( "a", "click", function() {
    $( ".modal-card-body" ).delegate( "a", "click", function() {    
        //alert( "User clicked on 'foo.'" );
        v = $(this).html();
        v2 = $(this).data('moecode');

        $("#sch_moecode").val(v2);
        $("#sch_name").val(v);

        //console.log('select-school =' + v + ' moecode=' + v2);
        $("#modalSchool").removeClass("is-active");
    });

    $("#clearCodeName").click(function(){
      $("#sch_moecode").val('');
      $("#sch_name").val('');
    });
    
    $("#move_student").submit(function(){
        
        //console.log("dateformat test " + dateformat.test($("#bdate").val()));
        if(!pid13format.test($("#person_id").val())){
            showAlert('danger', 'เลขบัตรประชาชน รูปแบบไม่ถูกต้อง');
            return false;
        }

        if(!dateformat.test($("#bdate").val())){
            showAlert('danger', 'วันเดือนปีเกิด รูปแบบไม่ถูกต้อง');
            return false;
        }

        if(!dateformat.test($("#receive").val())){
            showAlert('danger', 'วันเดือนปีรับเข้าเรียน รูปแบบไม่ถูกต้อง');
            return false;
        }

        //console.log("move_student submit test");
        //return false;
    });

    $("#showModal").click(function() {
      $("#modalSchool").addClass("is-active");  
    });

    $("#btn-modal-close").click(function() {
       $("#modalSchool").removeClass("is-active");
    });

}); //$(function() {
</script>