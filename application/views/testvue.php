<?php $this->load->view('template/head'); ?>

<section class="section">
  <div class="container">
    <h1 class="title">Section</h1>
    <h2 class="subtitle">
      A simple container to divide your page into <strong>sections</strong>, like the one you're currently reading Todd Garrigues หัวหน้าโครงการคู่ค้าของอินเทลยอมรับกับสำนักข่าว CRN ว่าปีที่ผ่านมาถูกคู่ค้าแสดงความไม่พอใจที่ไม่สามารถส่งมอบซีพียูให้ได้ และสัญญาว่าจะพยายามสื่อสารให้มากขึ้น

ข่าวซีพียูขาดตลาดเริ่มเป็นข่าวตั้งแต่ช่วงเดือนกันยายน เมื่ออินเทลออกมายอมรับผ่านช่องทางทางการของบริษัท ปัญหาใหญ่คือคู่ค้าจำนวนมากไม่รู้ว่าอินเทลจะส่งมอบซีพียูให้ได้เมื่อใด คู่ค้าบางส่วนระบุกับ CRN ว่าสั่งซีพียู Core i5-8500 ไปแต่ไม่ได้รับสินค้า จนต้องยอมประกอบเครื่องด้วย Core i5-8600 เพื่อให้ส่งมอบกับลูกค้าได้ตามสัญญาแม้ต้นทุนจะเพิ่ม
    </h2> 


<span class="icon has-text-info">
  <i class="fas fa-info-circle"></i>
</span>
<span class="icon has-text-success">
  <i class="fas fa-check-square"></i>
</span>
<span class="icon has-text-warning">
  <i class="fas fa-exclamation-triangle"></i>
</span>
<span class="icon has-text-danger">
  <i class="fas fa-ban"></i>
</span>


<link rel="stylesheet" type="text/css" href='<?php echo base_url("assets"); ?>/datatables.min.css' >
<script type="text/javascript" src='<?php echo base_url("assets"); ?>/datatables.min.js'></script>

<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
            </tr>
            <tr>
                <td>Garrett Winters</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>63</td>
                <td>2011/07/25</td>
                <td>$170,750</td>
            </tr>
            <tr>
                <td>Ashton Cox</td>
                <td>Junior Technical Author</td>
                <td>San Francisco</td>
                <td>66</td>
                <td>2009/01/12</td>
                <td>$86,000</td>
            </tr>
            <tr>
                <td>Cedric Kelly</td>
                <td>Senior Javascript Developer</td>
                <td>Edinburgh</td>
                <td>22</td>
                <td>2012/03/29</td>
                <td>$433,060</td>
            </tr>
            <tr>
                <td>Airi Satou</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>33</td>
                <td>2008/11/28</td>
                <td>$162,700</td>
            </tr>
            <tr>
                <td>Brielle Williamson</td>
                <td>Integration Specialist</td>
                <td>New York</td>
                <td>61</td>
                <td>2012/12/02</td>
                <td>$372,000</td>
            </tr>
            <tr>
                <td>Herrod Chandler</td>
                <td>Sales Assistant</td>
                <td>San Francisco</td>
                <td>59</td>
                <td>2012/08/06</td>
                <td>$137,500</td>
            </tr>
            <tr>
                <td>Rhona Davidson</td>
                <td>Integration Specialist</td>
                <td>Tokyo</td>
                <td>55</td>
                <td>2010/10/14</td>
                <td>$327,900</td>
            </tr>
            <tr>
                <td>Colleen Hurst</td>
                <td>Javascript Developer</td>
                <td>San Francisco</td>
                <td>39</td>
                <td>2009/09/15</td>
                <td>$205,500</td>
            </tr>
            <tr>
                <td>Sonya Frost</td>
                <td>Software Engineer</td>
                <td>Edinburgh</td>
                <td>23</td>
                <td>2008/12/13</td>
                <td>$103,600</td>
            </tr>

        </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </tfoot>
    </table>

  </div>

  <div class="container">
    <div class="columns is-mobile">
  <div class="column is-half is-offset-one-quarter">
    
    <div class="field">
      <p class="control has-icons-left has-icons-right">
        <input class="input" type="email" placeholder="Email">
        <span class="icon is-small is-left">
          <i class="fas fa-envelope"></i>
        </span>
        <span class="icon is-small is-right">
          <i class="fas fa-check"></i>
        </span>
      </p>
    </div>
    <div class="field">
      <p class="control has-icons-left">
        <input class="input" type="password" placeholder="Password">
        <span class="icon is-small is-left">
          <i class="fas fa-lock"></i>
        </span>
      </p>
    </div>
    <div class="field">
      <p class="control">
        <button class="button is-success">
          Login
        </button>
      </p>
    </div>

      </div>
  </div>
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function() {
      $('#example').DataTable();
  } );

</script>

<?php $this->load->view('template/foot'); ?>