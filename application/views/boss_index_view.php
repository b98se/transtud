
<section class="section">
  <div class="container">
    <h1 class="title">รายชื่อผู้ลงนาม</h1>
    <h2 class="subtitle">A simple container to divide your page into </h2> 

    <link rel="stylesheet" type="text/css" href='<?php echo base_url("assets"); ?>/datatables.min.css' >
    <script type="text/javascript" src='<?php echo base_url("assets"); ?>/datatables.min.js'></script>

    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>ที่</th>
                <th>ชื่อ-สกุล</th>
                <th>ตำแหน่ง</th>
                <th>ลายเซ็น</th>
                <th>สถานะ</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $html = "<tr>
                <td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>
                </tr>";
            foreach ($bosss as $i => $b) {
                echo sprintf($html, ($i+1), $b->b_name, $b->b_position, $b->b_position, $b->active);
            }
            ?>

        </tbody>
    </table>

  </div>

</section>

<script type="text/javascript">
  $(document).ready(function() {
      $('#example').DataTable();
  } );

</script>
