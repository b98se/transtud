<section class="hero is-light">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="column is-4 is-offset-4">
                <h3 class="title has-text-grey"><?php if(!$logged_in) echo 'เข้าสู่ระบบ'; ?></h3>
                <div class="box">
                    <figure class="avatar">
                        <img src='<?php echo base_url("assets"); ?>/images/obec.png'>
                    </figure>

                    <?php if(!$logged_in): ?>
                    <form method="get" name="login_form" id="login_form" action="<?php echo site_url('welcome/login') ?>">
                        <div class="field">
                            <div class="control">
                                <input class="input is-large" type="text" placeholder="Username" autofocus="" required="" name="usr">
                            </div>
                        </div>

                        <div class="field">
                            <div class="control">
                                <input class="input is-large" type="password" placeholder="Password"  required="" name="pwd">
                            </div>
                        </div>

                        <button class="button is-block is-info is-large is-fullwidth" >Login</button>
                    </form>
                    <?php else: ?>

                    <h1 class="title"><?php echo SYS_NAME ?></h1>
                    <h2 class="subtitle"><?php echo SYS_OFFICE_NAME_SHORT ?> </h2> 


                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    
    $("#login_form").submit(function (event) {

        // Stop form from submitting normally
        event.preventDefault();

        // Get some values from elements on the page:
        var $form = $(this),
                usr = $form.find("input[name='usr']").val(),
                pwd = $form.find("input[name='pwd']").val(),
                url = $form.attr("action");

        if (usr || pwd) {

            // Send the data using post
            var posting = $.post(url, {usr: usr, pwd: pwd});
            // Put the results in a div
            posting.done(function (data) {
                //console.log(data);
                if (data === "yes") {
                    window.location.href = "<?php echo site_url('/') ?>";
                    //showAlert('success', 'hooray !!!');
                } else {
                    showAlert('danger', 'username และ password ไม่ถูกต้อง !!!');
                }
            });

        } else {
            showAlert('warning', 'กรุณากรอก username และ password !!!');
        }
    });

</script>

